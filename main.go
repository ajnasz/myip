package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
)

var headers string

func getIP(addr string) string {
	ip, _, err := net.SplitHostPort(addr)
	if err != nil {
		ip = addr
	}

	res := net.ParseIP(ip)

	if res == nil {
		return ""
	}

	return res.String()
}

func getIPFromList(addresses []string) string {
	var ip string
	for _, addr := range addresses {
		if addr == "" {
			continue
		}

		ip = getIP(addr)

		if ip != "" {
			break
		}
	}

	return ip
}

func handler(headers []string, w http.ResponseWriter, r *http.Request) {
	addresses := []string{}
	for _, header := range headers {
		addresses = append(addresses, r.Header.Get(header))
	}

	addresses = append(addresses, r.RemoteAddr)

	ip := getIPFromList(addresses)

	if ip == "" {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, "error")
		return
	}

	fmt.Fprint(w, ip)
}

func init() {
	flag.StringVar(&headers, "headers", "x-real-ip,x-forwarded-for", "Comma separated list of headers to check for the ip address")
	flag.Parse()
}

func main() {
	headersList := strings.Split(headers, ",")
	fmt.Printf("List of headers: %v\n", headersList)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r.URL.Path)
		if r.URL.Path != "/" {
			w.WriteHeader(http.StatusNotFound)
			fmt.Fprintf(w, "Not found")
			return
		}
		if r.Method == http.MethodGet {
			fmt.Printf("%s\t%s\t%s\t%d\n", r.Method, r.Host, r.URL, r.ContentLength)
			handler(headersList, w, r)
		}
	})

	port := ":18081"
	envPort := os.Getenv("PORT")

	if envPort != "" {
		port = ":" + envPort
	}

	fmt.Printf("Listening on %s\n", port)
	log.Fatal(http.ListenAndServe(port, nil))
}
